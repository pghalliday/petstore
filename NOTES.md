# Notes

## Questions for Product Manager

### Orders

- Why do Orders have a quantity when it seems that
Pets refer to individual animals, and the Order only
refers to a single pet ID
- Orders don't seem to be associated with users and
can be deleted by anyone?
- Do customers enter orders or only the store staff?

### Users

- In the User model what is the userStatus?
  - Is it an access level for permissions?
- Create with list seems to be identical to create with array?
- The update user end point is not consistent with update pet 
  - I can see that this is because the user is referenced by name and the
  name can be changed, however wouldn't it make more sense to reference
  users by ID for this kind of operation? Particularly as otherwise the ID
  can also be changed
- Are customers also users, or only staff?

### Pets

- Upload image does not seem to have any error checking?
- Should upload image add to the photo URLs of the Pet?
  - How do I verify this?
- Categories appear to be unused
  - Are they supposed to replace tags as tags are 'deprecated'?
- The not authorized response for delete pet is not documented
so assuming a 401 Unauthorized status code should be returned
  
### Authentication

- How is user authentication supposed to work?
  - The login end point seems very insecure as it passes
  username and password in plain text in the query string
  - The login end point returns a session ID which is not documented
  - The logout endpoint makes no reference to a session ID header
  - No other end points seem to require an authenticated user
    - Although some User end points say they should require a logged in user,
    it is is still unclear how that should be implemented
  - There is an Api Key but this only seems to be referred to in the
  Delete Pet end point
  - There is an OAuth 2 authorize option, but it is also unclear how this
  integrates with the API

## Technical questions

### API

- How is authentication handled in Serenity / rest-assured?
  - This will depend on the authentication method I suppose
  
### Serenity BDD

- Can I set global headers for rest-assured like I do for the base URL?
- Tests seem to run very slowly is this because I am overcomplicating
with more the levels of abstraction?

### GitLab CI

- How do I link directly to the index of the report?
    - Not sure if this is easily achieved with GitLab CI
    - It should be possible to at least figure out the URL:

        ```
        https://pghalliday.gitlab.io/-/petstore/-/jobs/${JOB_ID}/artifacts/target/site/serenity/index.html
        ```

        So maybe I could attach that as a link somewhere?
