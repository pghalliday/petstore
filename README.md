# PetStore API tests

This project demonstrates how to test a Rest API using
Serenity BDD and Cucumber.

The starting point has been generated using version `2.3.2` of the
`net.serenity.bdd.serenity-cucumber-archetype` Maven archetype.
This means that it uses version `2.3.2` of Serenity BDD and Cucumber 6.

We are using the Screenplay pattern as described in
[The Serenity Book](https://serenity-bdd.github.io/theserenitybook/latest/serenity-screenplay-rest.html).

## Prerequisites

- JDK 11
- Maven 3.6.3

## Usage

Clone the repo with

```
git clone https://gitlab.com/pghalliday/petstore.git
```

Then run Maven with

```
mvn clean verify
```

## Project Configuration

The base URL for the API has been set in `src/test/resources/serenity.conf`

## Test Structure

The project files have been organised as follows:

```
src/
    main/ - There's nothing in here except a stub class to demonstrate where the API source code might go
    test/ - This is the test code
        java/
            org.example.petstore/
                acceptancetests/
                    AcceptanceTestSuite.java - The entry point for the test suite setting up the Cucumber runner
                actions/
                    <Capability> - End point actions associated with each capability are abstracted in here
                        <Action>.java - Serenity BDD Screenplay Task implementations for each end point action
                checks/
                    <Check>.java - Serenity BDD Screenplay Task implementations to abstract the checks and assertions
                models/
                    <Model>.java - POJO implementations to map to JSON requests and responses in the API endpoints
                steps/
                    <CapabilityStepDefinitions>.java - The top level Serenity BDD step definitions to map to the Gherkin clauses
        resources/
            features/
                <Capability>/ - The feature files describing the capability related API functions
                    <Feature>.feature - Individual feature files for each API function (end point)
                    narrative.txt - The capability description
            serenity.conf - The global configuration for tests (base URL)
```

## Test progress

Most Capabilities and Features are still to be implemented but the progress so far is:

| Capability | Feature | Status |
|------------|---------|--------|
| Pet | Add pet | Done |
| | Delete pet | Done |
| | Get pet | Done |
| | Find pets by status | |
| | Find pets by tags | |
| | Update pet | |
| | Update pet with form data| |
| | Upload image for pet | |
| Store | Place order | Done |
| | Delete order | |
| | Get order | |
| | Get inventory by status | |
| User | Add user | |
| | Create users with array | |
| | Create users with list | |
| | Delete user | |
| | Get user | |
| | Login | |
| | Logout | |
| | Update user | |

## Adding New Tests

To add new tests the procedure would be as follows:

1. For a new Capability create a folder under `src/test/resources/features/`
1. Add a `narrative.txt` file under the capability folder to describe the capability and it's purpose
1. For each Feature (end point) create a `<Feature>.feature` file to describe the different use cases of the end point
   - Add a description to document the intention of the feature
   - Create scenarios for the different success, failure and corner cases of the associated action
1. Create a step definitions file under `src/test/java/org/example/petstore/steps/` and implement the different Gherkin steps for the features there
   - Common steps that are used in multiple capabilities should be placed in `src/test/java/org/example/petstore/steps/CommonStepDefinitions.java` (eg. setting up the Screenplay Stage)
1. For each action (end point) abstract into a Serenity Screenplay Task class under `src/test/java/org/example/petstore/actions/<Capability>/`
1. For each check (assertion about the behaviour of the end point) abstract into a Serenity Screenplay Task class under `src/test/java/org/example/petstore/checks/`
1. New POJO models required for interacting with JSON endpoints should be placed in `src/test/java/org/example/petstore/models/`

Although many tests are still to be written, most of the `actions` and `models` have
already been implemented. Although a few corner cases probably remain.

## GitLab CI Integration

To demonstrate GitLab CI integration, this project has been
configured to run `mvn verify` and run the acceptance tests
when changes have been pushed to any branch.

The generated Serenity BDD report will be attached to each job as a directory
containing the full site. The report will be located under the `Job artifacts/Browse`
feature under the Job.

The report directory will be exposed as `Serenity BDD Report` in merge requests, and the JUnit report
will also be added there.