Feature: get a pet

  Store owners should be able to get the current details for a pet

  Background:
    Given a Pet Store owner named James

  Scenario: get an existing pet ID
    Given an existing pet ID
    When the store owner gets the pet by ID
    Then the store owner should receive a 200 response code
    And the store owner should receive the pet data

  Scenario: get a pet ID that does not exist
    Given a pet ID that does not exist
    When the store owner gets the pet by ID
    Then the store owner should receive a 404 response code

  Scenario: get a pet ID that is invalid
    Given a pet ID that is not valid
    When the store owner gets the pet by ID
    Then the store owner should receive a 400 response code
