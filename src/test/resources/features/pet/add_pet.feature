Feature: add a pet

  Store owners should be able to add pets to the store

  Background:
    Given a Pet Store owner named James

  Scenario Outline: add a pet
    Given a new pet with status <Status>
    When the store owner adds the pet
    Then the store owner should receive a 200 response code
    And the store owner should receive the pet data
    And the store owner should be able to see that the pet has been added
    Examples:
    | Status    |
    | available |
    | pending   |
    | sold      |

  Scenario: add a pet with an invalid status
    Given a new pet with an invalid status
    When the store owner adds the pet
    Then the store owner should receive a 405 response code

  Scenario: add an invalid pet
    When the store owner adds an invalid pet
    Then the store owner should receive a 405 response code
