Feature: delete a pet

  Store owners should be able to delete pets from the store

  Background:
    Given a Pet Store owner named James

  Scenario: delete an existing pet ID
    Given an existing pet ID
    And a valid API key
    When the store owner deletes the pet by ID
    Then the store owner should receive a 200 response code
    And the store owner should be able to see that the pet no longer exists in the database

  Scenario: delete a pet ID that does not exist
    Given a pet ID that does not exist
    And a valid API key
    When the store owner deletes the pet by ID
    Then the store owner should receive a 404 response code

  Scenario: delete a pet ID that is invalid
    Given a pet ID that is not valid
    And a valid API key
    When the store owner deletes the pet by ID
    Then the store owner should receive a 400 response code

  Scenario: delete a pet ID with an invalid API key
    Given an existing pet ID
    And an invalid API key
    When the store owner deletes the pet by ID
    Then the store owner should receive a 401 response code
