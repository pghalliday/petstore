Feature: place an order

  Customers should be able to place orders in the store

  Background:
    Given a Customer named Arthur

  Scenario Outline: place an order
    Given a new order
    And the order has status <Status>
    And the order has complete flag <Complete>
    When the customer places the order
    Then the customer should receive a 200 response code
    And the customer should receive the order data
    And the customer should be able to see that the order has been added
    Examples:
      | Status    | Complete |
      | placed    | false    |
      | approved  | false    |
      | delivered | true     |

  Scenario: place an order with an invalid status
    Given an order with an invalid status
    When the customer places the order
    Then the customer should receive a 405 response code

  Scenario: place an invalid order
    When the customer places an invalid order
    Then the customer should receive a 405 response code
