package org.example.petstore.steps;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.Steps;
import org.example.petstore.checks.CheckHttpStatusCode;
import org.example.petstore.checks.CheckPetResponse;
import org.example.petstore.models.Category;
import org.example.petstore.models.Pet;
import org.example.petstore.actions.pet.AddAnInvalidPet;
import org.example.petstore.actions.pet.AddPet;
import org.example.petstore.actions.pet.DeletePet;
import org.example.petstore.actions.pet.GetPet;

import static net.serenitybdd.screenplay.actors.OnStage.*;

public class PetStepDefinitions {

    @Steps
    private CommonStepDefinitions commonStepDefinitions;

    private Actor theOwner;

    private Category dogCategory = new Category(0, "Dog");
    private Category catCategory = new Category(1, "Cat");

    private Pet existingPet1 = new Pet(
            "George",
            "available",
            catCategory
    );
    private Pet existingPet2 = new Pet(
            "Penny",
            "pending",
            dogCategory
    );
    private Pet existingPet3 = new Pet(
            "Frank",
            "sold",
            catCategory
    );
    private Pet unknownPet = new Pet(
            "Emily",
            "pending",
            catCategory
    );

    private final String validApiKey = "special-key";
    private final String invalidApiKey = "bananas";
    private final String invalidId = "pineapples";

    private Pet currentPet;
    private String currentId;
    private String currentApiKey;

    private Pet newPet(String status) {
        return new Pet(
                "Greg",
                status,
                dogCategory
        );
    }

    @Before(order=2)
    public void initialize_data() {
        // We don't really control the data in the database
        // but lets do our best
        aNewActor().attemptsTo(
                // Ensure the unknown pet does not exist
                DeletePet.with(validApiKey, String.valueOf(unknownPet.getId())),
                // Add the existing pets
                AddPet.with(existingPet1),
                AddPet.with(existingPet2),
                AddPet.with(existingPet3)
        );
    }

    @Given("a Pet Store owner named {word}")
    public void a_store_owner_called(String name) {
        theOwner = theActorCalled(name);
    }

    @Given("a new pet with status {word}")
    public void a_new_pet(String status) {
        currentPet = newPet(status);
        currentId = String.valueOf(currentPet.getId());
    }

    @Given("a new pet with an invalid status")
    public void a_new_pet_with_an_invalid_status() {
        currentPet = newPet("invalid");
        currentId = String.valueOf(currentPet.getId());
    }

    @Given("a pet ID that does not exist")
    public void a_pet_id_that_does_not_exist() {
        currentPet = unknownPet;
        currentId = String.valueOf(currentPet.getId());
    }

    @Given("an existing pet ID")
    public void an_existing_pet_id() {
        currentPet = existingPet1;
        currentId = String.valueOf(currentPet.getId());
    }

    @Given("a pet ID that is not valid")
    public void an_invalid_pet_id() {
        currentId = invalidId;
    }

    @Given("a valid API key")
    public void a_valid_api_key() {
        currentApiKey = validApiKey;
    }

    @Given("an invalid API key")
    public void an_invalid_api_key() {
        currentApiKey = invalidApiKey;
    }

    @When("the store owner adds the pet")
    public void add_the_pet() {
        theOwner.attemptsTo(
                AddPet.with(currentPet)
        );
    }

    @When("the store owner adds an invalid pet")
    public void add_the_invalid_pet() {
        theOwner.attemptsTo(
                AddAnInvalidPet.action()
        );
    }

    @When("the store owner gets the pet by ID")
    public void get_the_pet_by_id() {
        theOwner.attemptsTo(
                GetPet.with(currentId)
        );
    }

    @When("the store owner deletes the pet by ID")
    public void delete_the_pet_by_id() {
        theOwner.attemptsTo(
                DeletePet.with(currentApiKey, currentId)
        );
    }

    @Then("the store owner should receive a {int} response code")
    public void should_receive_response_code(int code) {
        theOwner.attemptsTo(
                CheckHttpStatusCode.with(code)
        );
    }

    @Then("the store owner should receive the pet data")
    public void should_receive_the_pet_data() {
        theOwner.attemptsTo(
                CheckPetResponse.with(currentPet)
        );
    }

    @Then("the store owner should be able to see that the pet has been added")
    public void the_pet_should_have_been_added() {
        theOwner.attemptsTo(
                GetPet.with(currentId),
                CheckHttpStatusCode.with(200),
                CheckPetResponse.with(currentPet)
        );
    }

    @Then("the store owner should be able to see that the pet no longer exists in the database")
    public void should_no_longer_exist_in_the_database() {
        theOwner.attemptsTo(
                GetPet.with(currentId),
                CheckHttpStatusCode.with(404)
        );
    }

}
