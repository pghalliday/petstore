package org.example.petstore.steps;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.Steps;
import org.example.petstore.actions.store.GetOrder;
import org.example.petstore.actions.store.PlaceAnInvalidOrder;
import org.example.petstore.actions.store.PlaceOrder;
import org.example.petstore.checks.CheckHttpStatusCode;
import org.example.petstore.checks.CheckOrderResponse;
import org.example.petstore.models.Order;

import static net.serenitybdd.screenplay.actors.OnStage.*;

public class StoreStepDefinitions {

    @Steps
    private CommonStepDefinitions commonStepDefinitions;

    private Actor theCustomer;

    private Order currentOrder;
    private String currentStatus;
    private boolean currentComplete;

    private Order newOrder() {
        return new Order(
                100,
                5,
                null,
                "placed",
                false
        );
    }

    @Before(order=2)
    public void initialize_data() {
        // We don't really control the data in the database
        // but lets do our best
        aNewActor().attemptsTo(
        );
    }

    @Given("a Customer named {word}")
    public void a_customer_named(String name) {
        theCustomer = theActorCalled(name);
    }

    @Given("a new order")
    public void a_new_order() {
        currentOrder = newOrder();
    }

    @Given("the order has status {word}")
    public void order_has_status(String status) {
        currentOrder.setStatus(status);
    }

    @Given("the order has complete flag {word}")
    public void order_has_complete_flag(String complete) {
        currentOrder.setComplete(complete == "true");
    }

    @Given("an order with an invalid status")
    public void a_new_pet_with_an_invalid_status() {
        currentOrder = newOrder();
        currentOrder.setStatus("invalid");
    }

    @When("the customer places the order")
    public void place_the_order() {
        theCustomer.attemptsTo(
                PlaceOrder.with(currentOrder)
        );
    }

    @When("the customer places an invalid order")
    public void place_an_invalid_order() {
        theCustomer.attemptsTo(
                PlaceAnInvalidOrder.action()
        );
    }

    @Then("the customer should receive a {int} response code")
    public void should_receive_response_code(int code) {
        theCustomer.attemptsTo(
                CheckHttpStatusCode.with(code)
        );
    }

    @Then("the customer should receive the order data")
    public void should_receive_the_order_data() {
        theCustomer.attemptsTo(
                CheckOrderResponse.with(currentOrder)
        );
    }

    @Then("the customer should be able to see that the order has been added")
    public void the_order_should_have_been_added() {
        theCustomer.attemptsTo(
                GetOrder.with(currentOrder.getId()),
                CheckHttpStatusCode.with(200),
                CheckOrderResponse.with(currentOrder)
        );
    }
}
