package org.example.petstore.steps;

import io.cucumber.java.Before;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;

import static net.serenitybdd.screenplay.actors.OnStage.*;
import static net.serenitybdd.screenplay.actors.OnlineCast.whereEveryoneCan;

public class CommonStepDefinitions {

    private EnvironmentVariables environmentVariables;

    @Before(order=1)
    public void set_the_stage() {
        String baseUrl = environmentVariables.getProperty("restapi.baseurl");
        setTheStage(whereEveryoneCan(CallAnApi.at(baseUrl)));
    }

}
