package org.example.petstore.models;

import java.util.Date;

public class Order {

    // start with a random big ID and hopefully we won't
    // collide with anything anyone else is doing :s
    private static long currentId = 2480254395162339990L;

    private long id;
    private long petId;
    private int quantity;
    private Date shipDate;
    private String status;
    private boolean complete;

    public Order() {
    }

    public Order(long petId, int quantity) {
        this(petId, quantity, null, "placed", false);
    }

    public Order(long petId, int quantity, Date shipDate, String status, boolean complete) {
        this(currentId++, petId, quantity, shipDate, status, complete);
    }

    public Order(long id, long petId, int quantity, Date shipDate, String status, boolean complete) {
        this.id = id;
        this.petId = petId;
        this.quantity = quantity;
        this.shipDate = shipDate;
        this.status = status;
        this.complete = complete;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPetId() {
        return petId;
    }

    public void setPetId(long petId) {
        this.petId = petId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getShipDate() {
        return shipDate;
    }

    public void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }
}
