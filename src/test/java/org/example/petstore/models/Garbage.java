package org.example.petstore.models;

// This model represents invalid JSON data
// as no valid model has a String ID
public class Garbage {

    private String id;

    public Garbage() {
        this.id = "hello";
    }

    public String getId() {
        return id;
    }
}
