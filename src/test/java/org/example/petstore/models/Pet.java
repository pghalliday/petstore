package org.example.petstore.models;

import java.util.ArrayList;

public class Pet {

    // start with a random big ID and hopefully we won't
    // collide with anything anyone else is doing :s
    private static long currentId = 4652360400386944820L;

    private long id;
    private String name;
    private String status;
    private Category category;
    private final ArrayList<String> photoUrls = new ArrayList<>();
    private final ArrayList<Tag> tags = new ArrayList<>();

    public Pet() {
        this("", "", new Category());
    }

    public Pet(String name, String status, Category category) {
        this(currentId++, name, status, category);
    }

    public Pet(long id, String name, String status, Category category) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.category = category;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public ArrayList<String> getPhotoUrls() {
        return photoUrls;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }
}
