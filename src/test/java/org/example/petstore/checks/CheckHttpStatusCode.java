package org.example.petstore.checks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Ensure;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class CheckHttpStatusCode implements Task {

    private final int statusCode;

    public CheckHttpStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public static CheckHttpStatusCode with(int statusCode) {
        return instrumented(CheckHttpStatusCode.class, statusCode);
    }

    @Override
    @Step("{0} checks for HTTP status code #statusCode")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Ensure.that(
                        "status code should be #statusCode",
                        response -> response.statusCode(statusCode)
                )
        );
    }
}
