package org.example.petstore.checks;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.thucydides.core.annotations.Step;
import org.example.petstore.models.Pet;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static org.assertj.core.api.Assertions.assertThat;

public class CheckPetResponse implements Task {

    private final Pet pet;
    private final long petId;

    public CheckPetResponse(Pet pet) {
        this.pet = pet;
        this.petId = pet.getId();
    }

    public static CheckPetResponse with(Pet pet) {
        return instrumented(CheckPetResponse.class, pet);
    }

    @Override
    @Step("{0} checks the response for pet ID #petId")
    public <T extends Actor> void performAs(T actor) {
        Pet response = SerenityRest
                        .lastResponse()
                        .jsonPath()
                        .getObject("", Pet.class);
        assertThat(response).usingRecursiveComparison().isEqualTo(pet);
    }
}
