package org.example.petstore.checks;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.thucydides.core.annotations.Step;
import org.example.petstore.models.Order;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static org.assertj.core.api.Assertions.assertThat;

public class CheckOrderResponse implements Task {

    private final Order order;
    private final long orderId;

    public CheckOrderResponse(Order order) {
        this.order = order;
        this.orderId = order.getId();
    }

    public static CheckOrderResponse with(Order order) {
        return instrumented(CheckOrderResponse.class, order);
    }

    @Override
    @Step("{0} checks the response for order ID #orderId")
    public <T extends Actor> void performAs(T actor) {
        Order response = SerenityRest
                        .lastResponse()
                        .jsonPath()
                        .getObject("", Order.class);
        assertThat(response).usingRecursiveComparison().isEqualTo(order);
    }
}
