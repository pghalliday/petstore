package org.example.petstore.actions.store;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class GetInventoryByStatus implements Task {

    public GetInventoryByStatus() {
    }

    public static GetInventoryByStatus action() {
        return instrumented(GetInventoryByStatus.class);
    }

    @Override
    @Step("{0} requests the inventory numbers by status")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Get.resource("/store/inventory").with(
                        request -> request
                                .header("accept", "application/json")
                )
        );
    }
}
