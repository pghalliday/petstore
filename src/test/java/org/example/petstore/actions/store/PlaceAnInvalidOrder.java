package org.example.petstore.actions.store;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.thucydides.core.annotations.Step;
import org.example.petstore.models.Garbage;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class PlaceAnInvalidOrder implements Task {

    public PlaceAnInvalidOrder() {
    }

    public static PlaceAnInvalidOrder action() {
        return instrumented(PlaceAnInvalidOrder.class);
    }

    @Override
    @Step("{0} tries to place an order with ID #orderId")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to("/store/order").with(
                        request -> request
                                .header("accept", "application/json")
                                .header("Content-Type", "application/json")
                                .body(new Garbage())

                )
        );
    }
}
