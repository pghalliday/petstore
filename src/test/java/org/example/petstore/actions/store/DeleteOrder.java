package org.example.petstore.actions.store;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Delete;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class DeleteOrder implements Task {

    private final long orderId;

    public DeleteOrder(String apiKey, long orderId) {
        this.orderId = orderId;
    }

    public static DeleteOrder with(long orderId) {
        return instrumented(DeleteOrder.class, orderId);
    }

    @Override
    @Step("{0} tries to delete the pet with ID #orderId")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Delete.from("/store/order/{orderId}").with(
                        request -> request
                                .pathParam("orderId", orderId)
                                .header("accept", "application/json")
                )
        );
    }
}
