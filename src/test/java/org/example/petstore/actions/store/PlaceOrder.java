package org.example.petstore.actions.store;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.thucydides.core.annotations.Step;
import org.example.petstore.models.Order;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class PlaceOrder implements Task {

    private final Order order;
    private final long orderId;

    public PlaceOrder(Order order) {
        this.order = order;
        this.orderId = order.getId();
    }

    public static PlaceOrder with(Order order) {
        return instrumented(PlaceOrder.class, order);
    }

    @Override
    @Step("{0} tries to place an order with ID #orderId")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to("/store/order").with(
                        request -> request
                                .header("accept", "application/json")
                                .header("Content-Type", "application/json")
                                .body(order)
                )
        );
    }
}
