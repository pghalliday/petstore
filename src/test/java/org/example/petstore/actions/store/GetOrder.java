package org.example.petstore.actions.store;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class GetOrder implements Task {

    private final long orderId;

    public GetOrder(long orderId) {
        this.orderId = orderId;
    }

    public static GetOrder with(long orderId) {
        return instrumented(GetOrder.class, orderId);
    }

    @Override
    @Step("{0} tries to find the purchase order with ID #orderId")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Get.resource("/store/order/{orderId}").with(
                        request -> request
                                .pathParam("orderId", orderId)
                                .header("accept", "application/json")
                                .header("Content-Type", "application/json")
                )
        );
    }
}
