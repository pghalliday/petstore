package org.example.petstore.actions.pet;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class UpdatePetWithFormData implements Task {

    private final String petId;
    private final String name;
    private final String status;

    public UpdatePetWithFormData(String petId, String name, String status) {
        this.petId = petId;
        this.name = name;
        this.status = status;
    }

    public static UpdatePetWithFormData with(String petId, String name, String status) {
        return instrumented(UpdatePetWithFormData.class, petId, name, status);
    }

    @Override
    @Step("{0} tries to update a pet with ID #petId to have name '#name' and status '#status'")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to("/pet/{petId}").with(
                        request -> request
                                .pathParam("petId", petId)
                                .header("accept", "application/json")
                                .header("Content-Type", "application/x-www-form-urlencoded")
                                .formParam("name", name)
                                .formParam("status", status)
                )
        );
    }
}
