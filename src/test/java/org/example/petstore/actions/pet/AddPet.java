package org.example.petstore.actions.pet;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.thucydides.core.annotations.Step;
import org.example.petstore.models.Pet;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class AddPet implements Task {

    private final Pet pet;
    private final long petId;

    public AddPet(Pet pet) {
        this.pet = pet;
        this.petId = pet.getId();
    }

    public static AddPet with(Pet pet) {
        return instrumented(AddPet.class, pet);
    }

    @Override
    @Step("{0} tries to add a pet with ID #petId")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to("/pet").with(
                        request -> request
                                .header("accept", "application/json")
                                .header("Content-Type", "application/json")
                                .body(pet)
                )
        );
    }
}
