package org.example.petstore.actions.pet;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Delete;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class DeletePet implements Task {

    private final String apiKey;
    private final String petId;

    public DeletePet(String apiKey, String petId) {
        this.apiKey = apiKey;
        this.petId = petId;
    }

    public static DeletePet with(String apiKey, String petId) {
        return instrumented(DeletePet.class, apiKey, petId);
    }

    @Override
    @Step("{0} tries to delete the pet with ID #petId using API key '#apiKey'")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Delete.from("/pet/{petId}").with(
                        request -> request
                                .pathParam("petId", petId)
                                .header("accept", "application/json")
                                .header("api_key", apiKey)
                )
        );
    }
}
