package org.example.petstore.actions.pet;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Step;

import java.util.List;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class FindPetsByTags implements Task {

    private final List<String> tags;

    public FindPetsByTags(List<String> tags) {
        this.tags = tags;
    }

    public static FindPetsByTags with(List<String> tags) {
        return instrumented(FindPetsByTags.class, tags);
    }

    @Override
    @Step("{0} requests the list of pets with tags in #tags")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Get.resource("/pet/findByTags").with(
                        request -> request
                                .header("accept", "application/json")
                                .queryParam("tag", tags)
                )
        );
    }
}
