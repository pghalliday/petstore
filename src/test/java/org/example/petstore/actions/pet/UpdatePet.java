package org.example.petstore.actions.pet;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Put;
import net.thucydides.core.annotations.Step;
import org.example.petstore.models.Pet;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class UpdatePet implements Task {

    private final Pet pet;
    private final long petId;

    public UpdatePet(Pet pet) {
        this.pet = pet;
        this.petId = pet.getId();
    }

    public static UpdatePet with(Pet pet) {
        return instrumented(UpdatePet.class, pet);
    }

    @Override
    @Step("{0} tries to update a pet with ID #petId")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Put.to("/pet").with(
                        request -> request
                                .header("accept", "application/json")
                                .header("Content-Type", "application/json")
                                .body(pet)
                )
        );
    }
}
