package org.example.petstore.actions.pet;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class GetPet implements Task {

    private final String petId;

    public GetPet(String petId) {
        this.petId = petId;
    }

    public static GetPet with(String petId) {
        return instrumented(GetPet.class, petId);
    }

    @Override
    @Step("{0} tries to find the pet with ID #petId")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Get.resource("/pet/{petId}").with(
                        request -> request
                                .pathParam("petId", petId)
                                .header("accept", "application/json")
                )
        );
    }
}
