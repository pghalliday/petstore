package org.example.petstore.actions.pet;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.thucydides.core.annotations.Step;

import java.io.File;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class UploadImageForPet implements Task {

    private final String petId;
    private final String additionalMetadata;
    private final String imagePath;

    public UploadImageForPet(String petId, String additionalMetadata, String imagePath) {
        this.petId = petId;
        this.additionalMetadata = additionalMetadata;
        this.imagePath = imagePath;
    }

    public static UploadImageForPet with(String petId, String additionalMetadata, String imagePath) {
        return instrumented(UploadImageForPet.class, petId, additionalMetadata, imagePath);
    }

    @Override
    @Step("{0} tries to upload image from '#imagePath' with additional metadata '#additionalMetadata' for pet with ID #petId")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to("/pet/{petId}/uploadImage").with(
                        request -> request
                                .pathParam("petId", petId)
                                .header("accept", "application/json")
                                .header("Content-Type", "multipart/form-data")
                                .formParam("additionalMetadata", additionalMetadata)
                                .multiPart("file", new File(imagePath))
                )
        );
    }
}
