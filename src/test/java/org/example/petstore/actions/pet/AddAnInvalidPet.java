package org.example.petstore.actions.pet;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.thucydides.core.annotations.Step;
import org.example.petstore.models.Garbage;
import org.example.petstore.models.Pet;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class AddAnInvalidPet implements Task {

    public AddAnInvalidPet() {
    }

    public static AddAnInvalidPet action() {
        return instrumented(AddAnInvalidPet.class);
    }

    @Override
    @Step("{0} tries to add an invalid pet")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to("/pet").with(
                        request -> request
                                .header("accept", "application/json")
                                .header("Content-Type", "application/json")
                                .body(new Garbage())
                )
        );
    }
}
