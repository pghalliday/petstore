package org.example.petstore.actions.pet;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class FindPetsByStatus implements Task {

    private final String status;

    public FindPetsByStatus(String status) {
        this.status = status;
    }

    public static FindPetsByStatus with(String status) {
        return instrumented(FindPetsByStatus.class, status);
    }

    @Override
    @Step("{0} requests the list of pets with status '#status'")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Get.resource("/pet/findByStatus").with(
                        request -> request
                                .header("accept", "application/json")
                                .queryParam("status", status)
                )
        );
    }
}
