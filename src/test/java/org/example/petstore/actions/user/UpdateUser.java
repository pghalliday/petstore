package org.example.petstore.actions.user;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Put;
import net.thucydides.core.annotations.Step;
import org.example.petstore.models.User;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class UpdateUser implements Task {

    private final String username;
    private final User user;

    public UpdateUser(String username, User user) {
        this.username = username;
        this.user = user;
    }

    public static UpdateUser with(String username, User user) {
        return instrumented(UpdateUser.class, username, user);
    }

    @Override
    @Step("{0} tries to update the user with username #username")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Put.to("/user/{username}").with(
                        request -> request
                                .pathParam("username", username)
                                .header("accept", "application/json")
                                .header("Content-Type", "application/json")
                                .body(user)
                )
        );
    }
}
