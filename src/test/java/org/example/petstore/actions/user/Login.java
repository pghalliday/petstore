package org.example.petstore.actions.user;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Login implements Task {

    private final String username;
    private final String password;

    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public static Login with(String username, String password) {
        return instrumented(Login.class, username, password);
    }

    @Override
    @Step("{0} tries to login as user '#username' with password '#password'")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Get.resource("/user/login").with(
                        request -> request
                                .header("accept", "application/json")
                                .queryParam("name", username)
                                .queryParam("status", password)
                )
        );
    }
}
