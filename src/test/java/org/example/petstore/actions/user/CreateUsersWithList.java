package org.example.petstore.actions.user;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.thucydides.core.annotations.Step;
import org.example.petstore.models.User;

import java.util.List;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class CreateUsersWithList implements Task {

    private final List<User> users;
    private final int count;

    public CreateUsersWithList(List<User> users) {
        this.users = users;
        this.count = users.size();
    }

    public static CreateUsersWithList with(List<User> users) {
        return instrumented(CreateUsersWithList.class, users);
    }

    @Override
    @Step("{0} tries to create #count users from a list")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to("/user/createWithArray").with(
                        request -> request
                                .header("Content-Type", "application/json")
                                .header("accept", "application/json")
                                .body(users)
                )
        );
    }
}
