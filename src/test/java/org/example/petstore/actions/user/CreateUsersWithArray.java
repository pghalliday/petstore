package org.example.petstore.actions.user;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.thucydides.core.annotations.Step;
import org.example.petstore.models.User;

import java.util.List;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class CreateUsersWithArray implements Task {

    private final List<User> users;
    private final int count;

    public CreateUsersWithArray(List<User> users) {
        this.users = users;
        this.count = users.size();
    }

    public static CreateUsersWithArray with(List<User> users) {
        return instrumented(CreateUsersWithArray.class, users);
    }

    @Override
    @Step("{0} tries to create #count users from an array")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to("/user/createWithArray").with(
                        request -> request
                                .header("Content-Type", "application/json")
                                .header("accept", "application/json")
                                .body(users)
                )
        );
    }
}
