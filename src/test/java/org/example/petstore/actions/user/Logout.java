package org.example.petstore.actions.user;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Logout implements Task {

    public Logout() {
    }

    public static Logout action() {
        return instrumented(Logout.class);
    }

    @Override
    @Step("{0} tries to logout")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Get.resource("/user/logout").with(
                        request -> request
                                .header("accept", "application/json")
                )
        );
    }
}
