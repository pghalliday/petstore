package org.example.petstore.actions.user;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.thucydides.core.annotations.Step;
import org.example.petstore.models.User;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class AddUser implements Task {

    private final User user;
    private final long userId;

    public AddUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }

    public static AddUser with(User user) {
        return instrumented(AddUser.class, user);
    }

    @Override
    @Step("{0} tries to add a user with ID #userId")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to("/user").with(
                        request -> request
                                .header("accept", "application/json")
                                .header("Content-Type", "application/json")
                                .body(user)
                )
        );
    }
}
